## Summary

This Raspberry Pi case is designed to fit into a standard 3.5in hard drive slot. It works well with tool-less mounting systems, but I also added the ability for threaded nuts to be dropped in which work great as well and make this compatible with anything that a standard hard drive fit into.  
The nuts that I built around are the [McMaster-Carr 90480a007](https://www.mcmaster.com/#90480a007/) This cost me $8.32 for 100 nuts delivered to my door but I am sure a local hardware store would have the same thing far cheaper.

**[I Believe these nuts from Amazon will work as well.](https://amzn.to/2LP1vSA)**

## Print Settings

**Printer Brand:** Creality

**Printer:** Ender 3

**Rafts:** No

**Supports:** Yes

**Resolution:** 0.40 mm

**Infill:** 15%

### Notes

I have printed this part a few times and you can get away with doing no supports you just end up with some long bridges. I would advise against supports inside where the nuts go because it is nearly impossible to get those supports out correctly.

## How I Designed This

This was my first project using Fusion 360, and my first project built from the ground up for 3D printing. I started by building a profile sketch or “master sketch” of an HDD with the placement of the screw holes. Then I decided to make a rough Raspberry Pi model since I plan on many 3D printed projects for the Pi it would be easier to have a digital copy.

To make this model I used my Rough Raspberry Pi Model that you can find [here](http://blog.ansonbiggs.com/rough-raspberry-pi-model/), you can also see it in the picture below.

![Preview With Pi Installed](https://blog.ansonbiggs.com/content/images/2018/08/fusionPreview1.png#full)

Version 2 will be worked on soon since now I have experience with 3D printing and I know that things like the long supports that require long bridges are a big no-no. I'll also probably change the positioning of the Pi to be more in the middle. Any other feedback would be greatly appreciated.

## Download

[Download this Model on Thingiverse](https://www.thingiverse.com/thing:3021978)

[Check out this Project on my Gitlab](https://gitlab.com/MisterBiggs/MyModels/tree/master/HDD%20Pi%20Case%20V1)

[Check out this Project on my Blog](http://blog.ansonbiggs.com/hdd-pi-case-v1/)

## Images

![fusionPreview2](https://blog.ansonbiggs.com/content/images/2018/08/fusionPreview2.png)

![preview1-1](https://blog.ansonbiggs.com/content/images/2018/08/preview1-1.jpg)

![preview2](https://blog.ansonbiggs.com/content/images/2018/08/preview2.jpg)

![preview3](https://blog.ansonbiggs.com/content/images/2018/08/preview3.jpg)

![preview4-1](https://blog.ansonbiggs.com/content/images/2018/08/preview4-1.jpg)

![preview5-1](https://blog.ansonbiggs.com/content/images/2018/08/preview5-1.jpg)

![preview6](https://blog.ansonbiggs.com/content/images/2018/08/preview6.jpg)

![preview7](https://blog.ansonbiggs.com/content/images/2018/08/preview7.jpg)

![preview8](https://blog.ansonbiggs.com/content/images/2018/08/preview8.jpg)