# To Do

- [ ] Give all files human understandable names
- [ ] Make Banner in Canva

## Upload to the World Wide Web™

- [ ] Upload to [Thingiverse](https://www.thingiverse.com/thing:0/edit)
- [ ] Upload to [GrabCAD](https://grabcad.com/library/new)
- [ ] Upload to [Autodesk](https://gallery.autodesk.com/fusion360/my/projects/new)
- [ ] Upload to [MyMiniFactory](https://myminifactory.com/upload/object-upload)
- [ ] Upload to [Cults](https://cults3d.com/en/users/AnsonB/creations)
- [ ] Upload to [pinshape](https://pinshape.com/users/385523-anson-3d)
- [ ] Upload to [Ghost](https://blog.ansonbiggs.com/ghost)

## Social

- [ ] Index on [Google Webmasters](https://search.google.com/u/1/search-console?resource_id=https%3A%2F%2Fblog.ansonbiggs.com%2F)
- [ ] Queue content on [Buffer](https://buffer.com/app)
- [ ] Engage relevant content on [Twitter](https://twitter.com/)
- [ ] Pin in relevant boards [Pinterest](https://www.pinterest.com/)
- [ ] Post in relevant subreddit [Reddit](https://www.reddit.com/submit)

## Almost Done

- [ ] Push that git
- [ ] Pat yourself on the back