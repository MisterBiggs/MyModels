# Yet Another Test Suite Test Creation Instructions

## //TestName

//TestDescription

## Test Checklist

- [ ] Test Works
- [ ] Pictures of test working
- [ ] Pictures of test failing
- [ ] Create README.md with description of test, and other relevant info
- [ ] Mark done in Yet Another Test Suite Milestone