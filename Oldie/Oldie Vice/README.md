# Oldie Vice

## Tags

ThisOldTony, 3dprintable, 3dprinted, moving_parts, tool, useful, clamp, thread, household, design, fully_printable, jaws, pcb_vice, screw, string, vice, vise, pcb, electronics, clamp, bench_vice, mechanical_device

## Summary

Machinist Extraordinaire, [This Old Tony](https://www.youtube.com/channel/UC5NO8MgTQKHAWXp6z8Xl7yQ) recently put out a [video on a cool vice design](https://youtu.be/1mZhOlr5v-s) that I wanted to recreate. It is mechanically very simple and relies on a long rod with right-hand threads (clockwise) on one side and left-hand threads (counter-clockwise) on the other side. So, when the shaft is spun it causes the clamps to advance in opposite directions and at the same rate. This mechanism means that your workpiece is always dead center in the vice, and no matter the position of the clamps the vice takes up the same amount of area.

[![thisOldTony](https://blog.ansonbiggs.com/content/images/2018/09/thisOldTony.jpg)](https://www.youtube.com/watch?v=1mZhOlr5v-s)

This model was made entirely in [Fusion 360](https://blog.ansonbiggs.com/tag/fusion360/) using the [This Old Tony Video](https://youtu.be/1mZhOlr5v-s) as a reference. If you watch the video, This Old Tony does a great job explaining how the Vice works and showing off the unique mechanism that allows the Vice to function. My model is not a replica, and is designed from the ground up to be 3D Printed. 

![profileView](https://blog.ansonbiggs.com/content/images/2018/09/profileView.png)

### Some Assembly Required

This model comes in six different .stl files. The most challenging part about this model is the thread.stl which I go into detail about more later in the documentation. Assembly is very simple and requires no glue or anything that's not 3D printed unless you choose to use glue on some of the parts for better functionality which I would recommend. Below I have the assembly instructions embedded as a .pdf, but If that doesn't load for whatever reason, here is the rundown.

![CADvsReal](https://blog.ansonbiggs.com/content/images/2018/09/CADvsReal.jpg)

1. Thread on both clamps and  Thread each clamp all the way down the thread going in a back and forth motion on any areas that give resistance to make sure it threads smoothly once fully assembled. Try to make sure they are both the same distance from the middle to ensure your clamped workpiece is always centered.

2. Attach both of the thread ends, the ends can become loose very easily, so its recommended to apply a little glue to make sure it stays but is not necessary. If you glue, it can become impossible to get off later without damaging the thread.

3. Place assembled thread into the bottom of the base making sure that the clamps are in the grooves of the base. Holding the thread down in the middle of the base with a finger, do a full spin from the jaws being completely closed to completely open to make sure there are no issues with the thread, or bottom of the base and that everything slides smoothly without any snags. Then make sure jaws are spaced the same from the middle and click the top of the base into place locking it into place. Depending on tolerances of your printer the top may have a loose fit, but can easily be glued into place to make it rigid.


<iframe src="https://drive.google.com/file/d/1Cp3xd8xbypFQmKbg1gvCz9nmMIX70S_7/preview" width="90%" height="480"></iframe>

---

## Print Settings

**Printer Brand:** Creality

**Printer:** Ender 3

**Filament:** [RepRapper Red 3D Printer Filament PLA 1.75mm](https://amzn.to/2xHdjfO)

### In General

This assembly is a very easy print except for the thread. Below I will go into detail into what is recommended to get the best results for each part, but this is a very easy print, and every printer should be able to do the whole thing in about 10 hours. This print doesn't require any supports or rafts, but the orientation and layer height is important, and other details are what I will discuss below. The times and material cost are also taking into considering printing that particular .stl alone, but you should be able to print the whole thing at one time.

![modelPreview](https://blog.ansonbiggs.com/content/images/2018/09/modelPreview.jpg)

The assembly should be a very straightforward print, no supports should be necessary, and you shouldn't need any rafts either. Since this is a functional part, the more infill, the better, 20% would probably be the minimum. I also do 5 layers for the top, bottom, and perimeters.

![printPreview](https://blog.ansonbiggs.com/content/images/2018/09/printPreview.png)

![printPreview](https://blog.ansonbiggs.com/content/images/2018/09/printPreview.jpg)

### baseBottom.stl

Build time: 1 hour 55 minutes  
Filament length: 11384.9 mm  
Plastic weight: 34.23 g (0.08 lb)  

Layer Height: .2 mm  
External Fill Pattern: I liked the look of concentric, but it shouldn't matter.  

Super easy print and could easily be printed at .3 mm if you wanted to.

### baseTop.stl

Build time: 0 hours 19 minutes  
Filament length: 1151.6 mm  
Plastic weight: 3.46 g (0.01 lb)  

Layer Height: .2 mm  
External Fill Pattern: I liked the look of concentric, but it shouldn't matter.  
Infill: 30% Since this is the base of the model giving it more weight would be smart.   

Since this print has the tiny prongs anything higher than .2 mmm could make them super brittle. If you plan on gluing the model together, then this doesn't matter.


### xxxClamp.stl

(Printing Together)  
Build time: 2 hours 17 minutes  
Filament length: 7945.1 mm  
Plastic weight: 23.89 g (0.05 lb)  
Layer Height: .15 mm  

Since these have threads, I would print them at a small layer height, but since this thread is so large in diameter, you should be able to get away with a bigger build height.

### thread.stl

Build time: 6 hours 32 minutes  
Filament length: 5549.3 mm  
Plastic weight: 16.68 g (0.04 lb)  

raft: Your call  
Layer Height: .1 mm  
External Fill Pattern: Concentric  
Infill: 30% +  

The lower the layer height, the better the thread will turn out. The Ender 3 printed correctly at .1 mm even using a cheap filament. Do keep in mind that this only has about 150 mm^2 of area touching the build platform and if you have a printer where the build platform moves for the x-axis or y-axis 10 hours of moving back and forth can quickly knock it loose. So, if you have issues with prints adhering I would use a raft, and to be safe maybe even print the thread separate from the rest of the assembly.

### threadTwoEndCapDifficultPrint.stl

Build time: 6 hours 32 minutes  
Filament length: 5510.6 mm  
Plastic weight: 16.57 g (0.04 lb)  

Raft: Yes  
Layer Height: .1  
External Fill Pattern: Concentric  
Infill: 30% +  

The name makes it pretty clear that this is a problematic print for most prints, you have a long print with a meager 82mm^2 surface area touching the build platform.

### threadEnd.stl

Build time: 0 hours 39 minutes  
Filament length: 939.3 mm  
Plastic weight: 2.82 g (0.01 lb)  

Layer Height: .1 mm  
External Fill Pattern: Concentric  

Super simple and quick print. I used a lower layer height since it makes the knurling look better, but since this thread isn't meant to attach more than once, the quality doesn't matter much. 

### Entire Assembly at Once, Single Setting

Build time: 9 hours 53 minutes  
Filament length: 27399.0 mm  
Plastic weight: 82.38 g (0.18 lb)  

Layer Height: .15  
External Fill Pattern: Concentric  
Infill: 30%  

These are the settings to use for printing the entire assembly in under 10 hours. If you have issues with layer adhesion, I would print the thread.stl separate since it can be quite hard to print.

## Download

[Find this Model on my Thingiverse](https://www.thingiverse.com/thing:3118726)  
[Check out this Project on my GitLab](https://gitlab.com/MisterBiggs/MyModels/tree/master/Oldie/Oldie%20Vice)  
[Check out this Project on my Blog](https://blog.ansonbiggs.com/oldie-vice/)  
[Check out this Project on GrabCAD](https://grabcad.com/library/oldie-vice-1)  
[Check out this Project on Autodesk Community](https://gallery.autodesk.com/fusion360/projects/132168/oldie-vice)  
[Check out this Project on MyMiniFactory](https://www.myminifactory.com/object/3d-print-oldie-vice-75355)  
[Check out this Project on Cults 3D](https://cults3d.com/en/3d-model/tool/oldie-vice)  
[Check out this Project on pinshape](https://pinshape.com/items/47417-3d-printed-oldie-vice)  

## Images

![homeViewShadow](https://blog.ansonbiggs.com/content/images/2018/09/homeViewShadow.png)

![topBottomConnector](https://blog.ansonbiggs.com/content/images/2018/09/topBottomConnector.png)

![homeViewClose](https://blog.ansonbiggs.com/content/images/2018/09/homeViewClose.png)