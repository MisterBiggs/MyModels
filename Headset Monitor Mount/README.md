## Summary

This is a headphone mount that attaches to the top corner of your computer monitor using a 3D printed screw. It works perfectly for my current setup, an [ASUS VS248H-P 24 Monitor](https://amzn.to/2vf7EwT) and, [Steelseries H Wireless Headset](https://amzn.to/2OEL5K4). Using the parameters option (Modify > Parameters) in Fusion 360 this design can easily be adapted to any monitor/headset combination.

## Print Settings

**Printer Brand:** Creality

**Printer:** Ender 3

**Rafts:** No

**Supports:** Yes

**Resolution:** 0.40 mm

**Infill:** 15%, 4 Perimeter layers

**Print Notes:**

![Print Orientation for the Mount](https://blog.ansonbiggs.com/content/images/2018/08/printPreview.png)
The monitorHeadsetMount I printed upside down to minimize the number of supports. You still need some supports underneath the hook part but otherwise, this orientation does really well, even the threads printed perfectly on my Ender 3. Another key advantage of this orientation is it aligns the layers perpendicular to the mounting force and the force of hanging your headphones. 

The monitorHeadsetScrew prints pretty well on either the top or bottom faces.

## Download 

[Get this File on my Thingiverse](https://www.thingiverse.com/thing:3001913)
[Check out this Project on my GitLab](https://gitlab.com/MisterBiggs/MyModels/tree/master/Headset%20Monitor%20Mount)
[Check out this Project on my Blog](http://blog.ansonbiggs.com/headset-monitor-mount/)

## Pictures

![preview1-3](https://blog.ansonbiggs.com/content/images/2018/08/preview1-3.jpg)

![preview2-4](https://blog.ansonbiggs.com/content/images/2018/08/preview2-4.jpg)

![preview3-2](https://blog.ansonbiggs.com/content/images/2018/08/preview3-2.jpg)

![preview4-2](https://blog.ansonbiggs.com/content/images/2018/08/preview4-2.jpg)

![preview5-2](https://blog.ansonbiggs.com/content/images/2018/08/preview5-2.jpg)