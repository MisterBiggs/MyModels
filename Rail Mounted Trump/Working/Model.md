# HDD Pi Case V1

## Tags

`
computer, hard_drive, McMaster-Carr, Raspberry_Pi, Raspberry_pi_3, raspberry_pi_case,
`

## Summary

This Raspberry Pi case is designed to fit into a standard 3.5in hard drive slot. It works well with tool less mounting systems, but I also added the ability for threaded nuts to be dropped in which work great as well and make this compatible with anything that a standard hard drive fit into.  
The nuts that I built around are the [McMaster-Carr 90480a007](https://www.mcmaster.com/#90480a007/) This cost me $8.32 for 100 nuts delivered to my door but I am sure a local hardware store would have the same thing far cheaper.

## Print Settings

**Printer Brand:**

Creality

**Printer:**

Ender 3

**Rafts:**

No

**Supports:**

Yes

**Resolution:**

0.40 mm

**Infill:**

15%

**rail.stl:**

Build time: 1 hour 12 minutes
Filament length: 5737.7 mm
Plastic weight: 17.25 g (0.04 lb)

**trumpBust.stl:**

Build time: 3 hours 48 minutes
Filament length: 17912.3 mm
Plastic weight: 53.86 g (0.12 lb)

**Notes:**

\\

## How I Designed This

