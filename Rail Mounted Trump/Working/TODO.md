# To Do
- [x] Copy TODO.md to project Folder
- [x] Give all files human understandable names

## Upload to the World Wide Web™
- [ ] Upload to [Thingiverse](https://www.thingiverse.com/thing:0/edit)
- [ ] Upload to [GrabCAD](https://grabcad.com/library/new)
- [ ] Upload to [MyMiniFactory](https://myminifactory.com/upload/object-upload)

## Almost Done
- [ ] Push that git
- [ ] Pat yourself on the back