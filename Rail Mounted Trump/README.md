## Tags
alt_right, art, donald Donald_Trump, emperor, god, God_Emperor_Trump, picatinny, picatinny_adapter, Picatinny_rail, pic_rail, statue, Trump

## Summary

This part is printed in 2 separate parts, the [Donald Trump](https://www.thingiverse.com/thing:2071497) bust borrowed from [ZeroBeatPro](https://www.thingiverse.com/ZeroBeatPro/about), and the rail mounting part designed by me. 

For the rail, you have two different files to choose from. The first option is a custom size used by [this Airsoft gun](https://amzn.to/2O3Bz22) which is what I used as a case for my flamethrower, I believe the same dimensions work [The Boring Company *not a* Flamethrower](https://www.boringcompany.com/not-a-flamethrower/). The other included rail uses a standard Picatinny size. 

I really enjoyed this project because it brought me out of my comfort zone of parametric modeling and made me use [Blender](http://blog.ansonbiggs.com/tag/blender/) to modify the mesh of Trump's head.

## Print Settings

**Printer Brand:** Creality

**Printer:** Ender 3

**Rafts:** No

**Supports:** Yes

**Resolution:** 0.40 mm

**Infill:** 15%

### Rail

Build time: 1 hour 12 minutes  
Filament length: 5737.7 mm  
Plastic weight: 17.25 g (0.04 lb)  

The rail I printed on its side with supports on the cylinder. I'd say printing in this orientation is pretty important because you want the layers going vertical so the smaller pieces of the rail attachment are strong as possible because that's definitely the weak point.

![Rail Print Orientation](https://blog.ansonbiggs.com/content/images/2018/08/mountGCode.png)

### Trump Bust

Build time: 3 hours 48 minutes  
Filament length: 17912.3 mm  
Plastic weight: 53.86 g (0.12 lb)  

The Trump bust prints perfect without any supports, I used 15% infill but if you think this is going to get knocked around I would bump that number up a smidge.

![Trump GCode Preview](https://blog.ansonbiggs.com/content/images/2018/08/trumpGCode.png)

## Download

[Find this Model on my Thingiverse](https://www.thingiverse.com/thing:3026144)
[Check out this Project on my GitLab](https://gitlab.com/MisterBiggs/MyModels/tree/master/Rail%20Mounted%20Trump)
[Check out this Project on my Blog](http://blog.ansonbiggs.com/rail-mounted-trump/)

## Images

![assemblyPreview1-1](https://blog.ansonbiggs.com/content/images/2018/08/assemblyPreview1-1.jpg)

![bustPreview](https://blog.ansonbiggs.com/content/images/2018/08/bustPreview.jpg)

![railPreview](https://blog.ansonbiggs.com/content/images/2018/08/railPreview.jpg)
