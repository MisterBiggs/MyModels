# To Do
- [x] Copy TODO.md to project Folder
- [x] Give all files human understandable names

## Upload to the World Wide Web™
- [x] Upload to [Thingiverse](https://www.thingiverse.com/thing:0/edit)
- [x] Upload to [GrabCAD](https://grabcad.com/library/new)
- [x] Upload to [Autodesk](https://gallery.autodesk.com/fusion360/my/projects/new)
- [x] Upload to [MyMiniFactory](https://myminifactory.com/upload/object-upload)

## Almost Done
- [x] Push that git
- [x] Pat yourself on the back