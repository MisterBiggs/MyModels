## [GitLab](https://gitlab.com/MisterBiggs/MyModels)

This is the main repository that I work out of for my models. I work out of this repository so there are usually plenty of changes happening at any time. There are also files that I use to keep track of the progress and what sites I've posted on so you'll have to dig through those to get what you need.

I do my best to keep this neat, but if you catch a project in the middle of development its bound to be a mess. Surprisingly enough GitLab also has the functionality to preview a model in a browser as well, and it works way faster than any other embedding of a 3D model that I've seen.

## [Thingiverse](https://www.thingiverse.com/AnsonB/designs)

Thingiverse is by far the best community to share files with. It is not only the largest, but it also has the best tools and is easiest to upload to thanks to it supporting markdown for posts.

Thingiverse also has fantastic search and tagging tools so it's pretty easy to find something new while you are checking out my files.

## [GrabCAD](https://grabcad.com/anson.biggs-1)

GrabCAD doesn't seem to be a very 3D printing oriented community, they seem to keep things more professional and engineering. Regardless, I throw all my models on there because I like the site. 

## [MyMiniFactory](https://www.myminifactory.com/users/TheMisterBiggs)

MyMiniFactory only accepts 3D printable models, and you even have to prove its printable before it gets shared with the community, but the community seems pretty small. I still upload here but since I don't get many views I don't put much effort into the posts, I would recommend just going to my Thingiverse.

## [Autodesk Community](https://gallery.autodesk.com/fusion360/users/DVBMMB8LMZW2)

I post here just because I think its super professional place, and that's kinda the reason I'm doing all of this. That being said, my models get almost zero views here.

## [Cults](https://cults3d.com/en/users/AnsonB/creations)

Pretty small site but they retweet all my posts on twitter when I tag them about a new post to their site which is a nice little incentive to post there.

## [pinshape](https://pinshape.com/users/385523-anson-3d)

Very low traffic but they have a thingiverse importer, which is broken at the moment, that makes it easy to post there.