# Stringing Test

## Tags

YetAnotherTestSuite, YATS, 3dtest, print, printer, test, calibration, guide, testing, Bench-mark, Bench-marking, Benchie, Benchmark, Benchmarking, Benchy, calibrate, calibration, Calibration_part, calibration_test, printer_calibration, retraction_torture, test_print, torture, torture-test, easy, fast_print, quick_print, easy_print, easy_to_print, stringing, stringing_test, ooze, oozing

#### [Go to My full list of 3D Printing Troubleshooting Guides](http://blog.ansonbiggs.com/yet-another-test-suite/)  

*This is the beginning of a larger suite of test prints I am developing that can be used to calibrate your 3D printer cheaply, and quickly.*

## Summary

Stringing, also often referred to as Oozing, is when a print has small hairs or in extreme cases thick lines of the filament in between the outside perimeter of two print locations. Luckily cleanup for Stringing is generally pretty simple as long as the stringing isn't extreme. If you are unfortunate enough to have extreme stringing, then this guide and the included test print should get you up and running in no time.

*This model is fully parameterized if you have Inventor.*

![Inventor-Preview](https://blog.ansonbiggs.com/content/images/2018/08/Inventor-Preview.png#full)

<p style="text-align: center; font-style: italic;"> Preview of model in Inventor </p>

## Contents

This project contains a .stl file that has 3 Cones in a line. They are set up so that they test every length from 0mm to 15mm by default. There is also a .ipt file if you have Inventor and would like to make modifications to this. This model only uses one parameter, the longest distance you want stringing to be tested at and it automagically does the rest for you.

## Goals of This Print

The two major factors affecting stringing are **Retraction** and **Temperature**. Too high of a Temperature would cause a host of other issues in your prints so if you are mainly just experiencing stringing then Retraction is probably your issue. The included .stl tests stringing from 0mm to 15mm by default. Small strings often referred to as hairs are very common at small distances and are very easy to clean up after the print has finished. If stringing is prominent on the longer distances then I would look into the troubleshooting steps.


![Minimal-Stringing](https://blog.ansonbiggs.com/content/images/2018/08/Minimal-Stringing.jpg#full)
<p style="text-align: center; font-style: italic;"> Example of Good Stringing </p>


![Poor-Stringing](https://blog.ansonbiggs.com/content/images/2018/08/Poor-Stringing.jpg#full)
<p style="text-align: center; font-style: italic;"> Example of Poor Stringing </p>


### Retraction

Most every slicer has this ability, and thankfully this is more than likely your issue if stringing is an issue. Retraction causes your printer to pull some filament back when moving the print head in between parts of your print. The two parameters for this setting are **Distance** and **Speed**. Distance is how much filament gets pulled back, and speed is how fast it pulls it back. Mileage varies greatly by the printer so it's important to search online for some good baseline values to get started. The values I use for my Ender 3 are 6.20mm *Retraction Distance*, and 5100mm/min *Retraction Speed*. It is important that you do not set either of these values too crazy high because you can strip your filament causing it to not feed to your extruder.

### Temperature

*If stringing is your only print issue, then your temperature is probably fine.*

Too high of a temperature can make your filament really thin meaning it can come out the extruder tip just from a little gravity. This is pretty easily fixed by dropping your extruder temperature by 5ish degrees C.

### Clean-Up

Small amounts of stringing are perfectly fine and extremely easy to clean up after a print has finished. If there are only a few hairs I've had good luck with just rubbing them off with my finger, or if you have a large amount then using a heat gun or hair dryer usually melts them away pretty quickly. Superfine sandpaper works great as well.

Larger strings should be easy to fix with the above steps, but if you do end up with one then clipping them off with a pair of small diagonal wire cutters or worst case some fingernail clippers. Anything that can cut close to the walls of the print will work great.

## Print Settings

This model shouldn't require rafts or infill, but it may be beneficial to print it at your normal printing settings to be 100% sure everything is operating as it should.

## Download

[Get this File on my Thingiverse](https://www.thingiverse.com/thing:3039763)

[Get this File on my GitLab](http://bit.ly/stringingTest)