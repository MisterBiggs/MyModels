Yet Another Test Suite is a suite of tests for calibrating your 3D Printer. The goal of this specific suite of tests is that they are easy to read, quick to print, and cheap. This page is made to act as a directory for finding all of my tests in one place.

---

## Table Of Contents

#### [Bridge Test](https://blog.ansonbiggs.com/yet-another-test-suite-bridging-test/)  

#### [Stringing Test](http://blog.ansonbiggs.com/yet-another-test-suite-stringing-test/)

#### [Overhang Test](https://blog.ansonbiggs.com/yet-another-test-suite-overhang-test/)

#### [Support Test](https://blog.ansonbiggs.com/yet-ansother-test-suite-support-test)

#### [Sharp Corner Test]() [- In Progress -]  

#### [Tolerance Test]() [- In Progress -]  

#### [Screw Test]() [- In Progress -]  

---

## Bridge Test

![An example of poor 3D Printed Bridging](https://blog.ansonbiggs.com/content/images/2018/08/poorBridging.jpg)

The two major factors that come into play when printing a good bridge are **Temperature** and **Print Speed**. The lower both of these respective values are the better your bridges are likely to succeed. The goal of this project is that its a test that prints very fast, the full test should print in under 45 minutes.

This model is fully parameterized if you have Inventor.

#### Continued 

[View the full writeup On my Blog](https://blog.ansonbiggs.com/yet-another-test-suite-bridging-test/)

[View this Project on my Thingiverse](https://www.thingiverse.com/thing:3036666)

[View this Project on my GitLab](http://bit.ly/AnsonBridgeTest)

---

## Stringing Test

![Poor-vs-Good-Stringing](https://blog.ansonbiggs.com/content/images/2018/08/Poor-vs-Good-Stringing.png)

The two major factors affecting stringing are **Retraction** and **Temperature**. Too high of a Temperature would cause a host of other issues in your prints so if you are mainly just experiencing stringing then Retraction is probably your issue. The included .stl tests stringing from 0mm to 15mm by default. Small strings often referred to as hairs are very common at small distances and are very easy to clean up after the print has finished. If stringing is prominent on the longer distances then I would look into the troubleshooting steps.

#### Continued 

[View the full writeup On my Blog](http://blog.ansonbiggs.com/yet-another-test-suite-stringing-test/)

[View this Project on my Thingiverse](https://www.thingiverse.com/thing:3039763)

[View this Project on my GitLab](http://bit.ly/stringingTest)

---

## Overhang Test

![underView1](https://blog.ansonbiggs.com/content/images/2018/08/underView1.jpg)

No one likes using supports so it is essential to know the limitations of your printer so you can use a minimal amount of support and still get great looking models. This model tests 3 different kinds of overhangs in one. It tests concave, convex, and flat surfaces so you can see how your printer is able to do each one. Generally, concave overhangs print better than convex so this print is great for visualizing this. The idea behind this is that printing outside of parametric models usually has very weird shapes which normal flat tests don't test for.

#### Continued 

[View the full writeup On my Blog](https://blog.ansonbiggs.com/yet-another-test-suite-overhang-test/)

[View this Project on my Thingiverse](https://www.thingiverse.com/thing:3042444)

[View this Project on my GitLab](http://bit.ly/OverhangTest)

---

## Support Test

![add an image]()

filler
paragraph

#### Continued

[Blog Link]

[Thingiverse Link]

[Gitlab Link]

---