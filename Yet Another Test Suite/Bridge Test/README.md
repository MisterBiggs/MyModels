# Bridge Test

## Tags

YetAnotherTestSuite, YATS, 3dtest, print, printer, test, calibration, guide, testing, Bench-mark, Bench-marking, Benchie, Benchmark, Benchmarking, Benchy, calibrate, calibration, Calibration_part, calibration_test, printer_calibration, printer_torture_test, retraction_torture, test_print, torture, torture-test, easy, fast_print, quick_print, easy_print, easy_to_print, bridge, bridging, quick, fast, small, bridge_test, bridging_test

*This is the beginning of a larger suite of test prints I am developing that can be used to calibrate your 3D printer cheaply, and quickly.*

[Find a full write up and other tests on my blog](http://blog.ansonbiggs.com/yet-another-test-suite/)

## Summary

The two major factors that come into play when printing a good bridge are **Temperature** and **Print Speed**. The lower both of these respective values are the better your bridges are likely to succeed. The goal of this project is that its a test that prints very fast, the full test should print in under 45 minutes.

*This model is fully parameterized if you have Inventor.*

## Contents

This project contains two .stl files that are ready to be sliced and printed. bridgeTest.stl is a complete test with 4 different lengths. oneBridge.stl contains only the longest of the bridges at 25mm. The other files is bridgeTest.ipt this is a parameterized inventor file for changing the parameters of the test for anyone who wishes to do that, but I think the default setup is good.

## Goals of This Print

This print is designed to be fast to help you troubleshoot any issues your printer may encounter as your bridges increase in length. By default, the bridge spans are 10mm, 15mm, 20mm, and 25 mm. Another thing to note concerning this print is by default the bridge is 3mm above the print bed. In most cases, I think this is enough to avoid heat coming off your print bed to affect the bridge quality but your results may vary and it is something to consider. 

From what I've seen, horizontal bridges will never quite match the quality of a normal print operation, but you can get pretty close. You will always have very defined lines, but if done correctly these should be connected to the print pretty well and shouldn't be an issue outside of being a slight eyesore on some prints.

### Extrusion

If your extrusion is off it will be very obvious. Over extrusion is going to mean that you have a bridge that may sag lower than it should, meaning any layers above it aren't going to have anything to support them which will cause structural and visual issues above the bridge for a few layers before your printer hopefully recovers.

The other issue is under extrusion which in a worst case scenario means your bridge will break in the middle of the process leading to a huge mess under the bridge and may cause even more issues than over extrusion.

### Temperature

The key element in bridging is that your cooling melted plastic in place while it is suspended in the air. This means its important to make sure your temperature isn't higher than its required to be, and that your print head fans are running at full to ensure the filament hardens as quickly as possible once it leaves the print head.

### Good Bridging
![Example of Good 3D Printing Bridging](/content/images/2018/08/goodBridging.jpg#full)

### Poor Bridging
![Example of Poor 3D Printing Bridging](/content/images/2018/08/poorBridging.jpg#full)

---

## Print Settings

### Rafts

I didn't use any, but if you're having issues with the print not sticking since it does have a very small surface area on the bed, or if your having an issue with the bed giving off too much heat I would imagine that a raft would help.

### Supports

Definitely defeats the purpose of this print unless you're testing your support settings.

### Resolution

0.40 mm

### Infill

I would do something low like 10%-15%, more importantly, I would do 3+ exterior shells to ensure you are seeing how the bridge works with more layers on it.

## Download

[View this Project on my Thingiverse](https://www.thingiverse.com/thing:3036666)

[View this Project on my GitLab](http://bit.ly/AnsonBridgeTest)