# To Do
- [x] Copy TODO.md to project Folder

- [ ] overhang test
- [x] Bridging test
- [ ] Stringing test
- [ ] Sharp corner test
- [ ] Tolerance test
- [ ] Screw Test

- [ ] Give all files human understandable names

## Upload to the World Wide Web™
- [ ] Upload to [Thingiverse](https://www.thingiverse.com/thing:0/edit)
- [ ] Upload to [GrabCAD](https://grabcad.com/library/new)
- [ ] Upload to [Autodesk](https://gallery.autodesk.com/fusion360/my/projects/new)
- [ ] Upload to [MyMiniFactory](https://myminifactory.com/upload/object-upload)
- [ ] Upload to [Ghost](blog.ansonbiggs.com/ghost)

## Almost Done
- [ ] Push that git
- [ ] Pat yourself on the back