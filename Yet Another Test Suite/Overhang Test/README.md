# Stringing Test

## Tags

YetAnotherTestSuite, YATS, 3dtest, print, printer, printer_calibration, test, calibration, guide, testing, Benchie, Benchmark, Benchmarking, Benchy, calibrate, calibration, Calibration_part, calibration_test, printer_calibration, retraction_torture, test_print, torture, torture_test, easy, fast, fast_print, quick_print, easy_print, easy_to_print, stringing, overhang_test

#### [Go to My full list of 3D Printing Troubleshooting Guides](https://blog.ansonbiggs.com/yet-another-test-suite/)

*This is the beginning of a larger suite of test prints I am developing that can be used to calibrate your 3D printer cheaply, and quickly.*

## Summary

No one likes using supports so it is essential to know the limitations of your printer so you can use a minimal amount of support and still get great looking models. This model tests 3 different kinds of overhangs in one. It tests concave, convex, and flat surfaces so you can see how your printer is able to do each one. Generally, concave overhangs print better than convex so this print is great for visualizing this. The idea behind this is that printing outside of parametric models usually has very weird shapes which normal flat tests don't test for.

*This model is fully parameterized if you have Inventor.*

## Project Contents

This project contains a .stl and a .ipt Inventor file. By default, the .stl is pretty small and should be printable in under 45 minutes. If you want to print it larger to make any issues more prevalent then you should easily be able to scale the model in your slicer without any issues. The model also has a small notch on the back at the 50-degree mark, this is so it's easy to tell when your at this point because this is where most printers begin running into issues with overhangs.

![inventorPreview-1](https://blog.ansonbiggs.com//content/images/2018/08/inventorPreview-1.png#full)
![modelProfile](https://blog.ansonbiggs.com//content/images/2018/08/modelProfile.png#full)
<p style="text-align: center; font-style: italic;"> Preview of the file in Inventor </p>

## Goals of This Print

This print is designed to be small, fast, and easily readable to make it easy as possible for you to troubleshoot overhang issues. *[If you're having extreme overhang issues I would recommend starting with my Bridge Test.](https://blog.ansonbiggs.com/yet-another-test-suite-bridging-test/)* Bridging and Overhangs are highly correlated, but getting beautiful overhangs takes more fine tuning than successful bridges. 

This test is a profile with 2 arcs and a flat surface that is lofted up at increasing angles until you end up parallel with the ground. The reasonable expectation of any printer is that it is able to print perfectly until about 45 to 50 degrees before it runs into issues. The model has a small notch on the back to indicate when you go to 50 degrees, so if you have any issues before this point then your issue isn't Overhang, [its something else that I probably have a guide on.](https://blog.ansonbiggs.com/yet-another-test-suite/) Its hard to capture in pictures with my black filament, but the changes in degrees are pretty easy to see with the print in your hands. each change is 10 degrees down until you get to zero (parallel with the ground).

![underView1](https://blog.ansonbiggs.com//content/images/2018/08/underView1.jpg#full)
<p style="text-align: center; font-style: italic;"> You can see the print having worse issues as the print angle gets more agressive</p>

### Eliminating Overhangs

It is important to note that for most prints the best way to fix overhangs is to not have them at all. Usually rotating a model, or adding a chamfer or fillet can decrease an angle making your print easier for your printer, while saving filament, and in some cases actually making your print stronger.

### Speed

Slowing your printer down means that the filament has more time to cool down where it was placed, and not drag or fall. Printers can have vastly different speeds so it's important to search online for numbers that make sense for you, then slowly change them from there.

### Temperature

Overhangs print more and more material over thin air as the angle decreases. This means its important to make sure your temperature isn't higher than its required to be, and that your print head fans are running at full to ensure the filament hardens as quickly as possible once it leaves the print head.

---

## Print Settings

### Rafts

This print should be just fine without a raft. If you are having problems with the print not sticking to the bed then I would look up guides on troubleshooting that.

### Resolution

Whatever you usually print at. Smaller layer height should make overhangs turn out better.

### Infill

At the default size you shouldn't need any infill, but if you scale it up then I would recommend using your normal infill for prints just to keep as many variables the same as you can. I generally stick to around 15%

## Download

[View this Project on my Thingiverse](https://www.thingiverse.com/thing:3042444)

[View this Project on my GitLab](http://bit.ly/OverhangTest)