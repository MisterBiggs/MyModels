bezelWidth = 20.8;
bezelDepth	= 27.5;
headphoneBandThickness = 35;
headphoneBandHeight = 12;
screwDiameter = 17;
tolerance = 1.05;
screwDepth = 17;
screwHead = 5;
headphoneSupport = 2;
wallThickness = 5;

difference(){
    linear_extrude(bezelDepth + wallThickness)
        polygon([[0,0],
            [bezelWidth + wallThickness,-5],
            [bezelWidth + wallThickness,bezelWidth + wallThickness],
            [0,bezelWidth + wallThickness]
            ]);

    linear_extrude([bezelWidth - wallThickness]){
        translate([wallThickness,1,2]){ 
            polygon([
            [0,0],
            [bezelWidth,0],
            [bezelWidth,bezelWidth],
            [0,bezelWidth]
            ]);
        }
    }
}
