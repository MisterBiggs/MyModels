## Summary

A rough, blocky, minimal, but dimensional correct model of a [Raspberry Pi 3 Model B+](https://amzn.to/2AI34wa). I use this to quickly measure parts of the Pi whenever I'm working on a Pi-related project in CAD. I included the **.f3d** file which should work best since the model was designed in Fusion, but I also included a **.step** file for use in every other CAD software, as far as I know, feel free to ask for a different file type.

## Printing

**I don't see any reason in printing this but you do you.**

## Download

[Get this model on my Thingiverse](https://www.thingiverse.com/thing:3011653)
[Get this Project on my Gitlab](https://gitlab.com/MisterBiggs/MyModels/tree/master/Rough%20Pi%20Model)
[Get this Project on my Blog](http://blog.ansonbiggs.com/rough-raspberry-pi-model/)

[Direct Download .stl](http://bit.ly/RoughRaspstl)  
[Direct Download .step](http://bit.ly/RoughRaspstep)  
[Direct Download .f3d](http://bit.ly/RoughRaspf3d)  

## Pictures

![Dimension-Pi](https://blog.ansonbiggs.com/content/images/2018/08/Dimension-Pi.png)

![Green-Pi](https://blog.ansonbiggs.com/content/images/2018/08/Green-Pi.png)